//
//  ViewController.h
//  doodlejump_clone
//
//  Created by Vikram on 2/17/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Brick.h"
#import <AVFoundation/AVFoundation.h>

int jumpvaluex,jumpvaluey;
int basepoint;


@interface ViewController : UIViewController

{

    
    NSTimer *fallingtimer, *splashscreentimer;
    AVAudioPlayer *audioPlayer;
    
}
@property (nonatomic, strong) NSMutableArray *bricks;
- (IBAction)ShiftDetect:(UIGestureRecognizer *)sender;


@end

