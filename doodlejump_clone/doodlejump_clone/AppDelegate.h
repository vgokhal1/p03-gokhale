//
//  AppDelegate.h
//  doodlejump_clone
//
//  Created by Vikram on 2/17/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

