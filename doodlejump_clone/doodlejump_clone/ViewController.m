//
//  ViewController.m
//  doodlejump_clone
//
//  Created by Vikram on 2/17/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *jumpercharacter;
@property (strong, nonatomic) IBOutlet UIView *gameview;
@property (weak, nonatomic) IBOutlet UIView *SplashScreenView;
@property (weak, nonatomic) IBOutlet UIImageView *SplashImage;


@end

@implementation ViewController
@synthesize bricks;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    splashscreentimer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(splashtimercode) userInfo:nil repeats:NO];
    
    //_SplashScreenView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pikachu4.jpg"]];
    
    self.SplashScreenView.hidden=NO;
    self.gameview.hidden=YES;
    
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)splashtimercode
{
    
   fallingtimer=[NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(fallingcode) userInfo:nil repeats:YES];
    
    basepoint=CGRectGetMaxY(_gameview.frame)-40;
    
    _gameview.backgroundColor = [UIColor yellowColor];
    [self makeBricks:nil];
    self.SplashScreenView.hidden=YES;
    self.gameview.hidden=NO;
}

-(void)fallingcode
{
    CGRect bounds = [_gameview bounds];
    
    jumpvaluey=jumpvaluey-5;
    _jumpercharacter.center=CGPointMake(_jumpercharacter.center.x+jumpvaluex, _jumpercharacter.center.y-jumpvaluey);
    
    
    
    if((_jumpercharacter.center.y)>=(basepoint-15))
    {
        //jumpvalue=0;
         _jumpercharacter.center=CGPointMake(_jumpercharacter.center.x,basepoint-10);
        jumpvaluey=50;
        
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"PikaJump" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
        
        //(_jumpercharacter.center.y)==CGRectGetMaxY(_gameview.frame)-40)
    }
     
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    
    
    if (_jumpercharacter.center.x < CGRectGetMinX(_gameview.frame))
        _jumpercharacter.center=CGPointMake(_jumpercharacter.center.x+CGRectGetMaxX(_gameview.frame), _jumpercharacter.center.y);
    
    if (_jumpercharacter.center.x > CGRectGetMaxX(_gameview.frame))
        _jumpercharacter.center=CGPointMake(_jumpercharacter.center.x-CGRectGetMaxX(_gameview.frame), _jumpercharacter.center.y);
    
    
    CGFloat x=_jumpercharacter.frame.origin.x;
    x=x+_jumpercharacter.frame.size.width/2;
    CGFloat y=_jumpercharacter.frame.origin.y;
    //CGPoint p = CGPointMake(x, y);
    
    CGRect pRect = CGRectMake(x,y,20,20);

    
    for (Brick *brick in bricks)
        {
            //CGRect b = [brick frame];
            //CGRectContainsPoint(b, _jumpercharacter.center)
            //CGRectIntersectsRect(brick.frame, _jumpercharacter.frame)
            
            if (CGRectIntersectsRect(brick.frame, pRect))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                jumpvaluey=50;
                //jumpvaluey+=10;
                //basepoint=brick.center.y;
                
                
                NSString *path = [[NSBundle mainBundle]
                                  pathForResource:@"PikaJump" ofType:@"mp3"];
                audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                               [NSURL fileURLWithPath:path] error:NULL];
                [audioPlayer play];
            }
        }
    
    if(_jumpercharacter.center.y < (CGRectGetMaxY(_gameview.frame)/2))
    {
        
        for (Brick *brick in bricks)
        {
            [brick setCenter:CGPointMake(brick.center.x, brick.center.y+50)];
            
            if(brick.center.y > (CGRectGetMaxY(_gameview.frame)))
            {
                [brick setCenter:CGPointMake(rand() % (int)(bounds.size.width), rand() % (int)(bounds.size.height))];
            }
        }
        
        
        NSLog(@"More Than Half!!!");
    }
  
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [_gameview bounds];
    float width = bounds.size.width * .2;
    float height = 10;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 15; ++i)
    {
        Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [b setBackgroundColor:[UIColor darkGrayColor]];
        [_gameview addSubview:b];
        [b setCenter:CGPointMake(rand() % (int)(bounds.size.width), rand() % (int)(bounds.size.height))];
        [bricks addObject:b];
    }
}

- (IBAction)ShiftDetect:(UIGestureRecognizer *)sender {
    
    UISwipeGestureRecognizerDirection dir=[(UISwipeGestureRecognizer *)sender direction];
    
    switch (dir) {
        case UISwipeGestureRecognizerDirectionRight:
            [_jumpercharacter setImage:[UIImage imageNamed: @"pikachuright.jpg"]];
            jumpvaluex+=5;
            if(jumpvaluex>5)
                jumpvaluex=5;
            break;
            
        case UISwipeGestureRecognizerDirectionLeft:
            [_jumpercharacter setImage:[UIImage imageNamed: @"pikachuleft.jpg"]];
           jumpvaluex-=5;
            if(jumpvaluex<-5)
                jumpvaluex=-5;
            break;
        
            
        default:
            break;
    }
    
    
}
@end
